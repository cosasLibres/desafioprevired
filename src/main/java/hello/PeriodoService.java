package hello;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

@Service
public class PeriodoService {

	public String validarTramosFaltantes(Periodo periodo) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		StringBuilder resultado = new StringBuilder();
		
		List<String> periodosFaltantes = new ArrayList<String>();
		List<String> periodos = new ArrayList<String>();
		periodos.add("1969-03-01");
		periodos.add("1969-05-01");
		periodos.add("1969-09-01");
		periodos.add("1970-01-01");		
		
		resultado.append("fecha creación: "+sdf.format(periodo.getFechaCreacion())+"\n");
		resultado.append("fecha fin: "+sdf.format(periodo.getFechaFin())+"\n");
		
		String fechaRecibidas = periodos.stream().map(n -> String.valueOf(n)).collect(Collectors.joining(",", "", ""));
		
		resultado.append("fechas recibidas: "+fechaRecibidas+"\n");
		resultado.append("fechas faltantes: ");
		
		Date fechaActual = sdf.parse(periodo.getFechaCreacion());
		
		//itero desde la fecha de inicio hasta la fecha fin 
		do {
			if(!periodos.contains(sdf.format(fechaActual))) {
				periodosFaltantes.add(sdf.format(fechaActual));
			}
			
			//uso calendar para poder sumarle un mes 
			Calendar fechaCalendar = Calendar.getInstance();
			fechaCalendar.setTime(fechaActual);
			fechaCalendar.add(Calendar.MONTH, 1);
			
			fechaActual = fechaCalendar.getTime();
			
		} while (fechaActual.before(sdf.parse(periodo.getFechaFin())));
		
		
		String fechaFaltantes = periodosFaltantes.stream().map(n -> String.valueOf(n)).collect(Collectors.joining(",", "", ""));
		resultado.append(fechaFaltantes);
		
		return resultado.toString();
	}
	
}
